package com.fan.gupaolearn.myspring.demo.service.impl;

import com.fan.gupaolearn.myspring.demo.service.TestService;
import com.fan.gupaolearn.myspring.framework.annotation.MyService;

import java.util.Arrays;

@MyService
public class TestServiceImpl implements TestService {

    @Override
    public String Hello(String name) {
        return "Hello : " + name;
    }

    public static void main(String[] args) {
        int[] array = {2, 1, 3};
        // sort 从小到大排序 -> 1、2、3
        Arrays.sort(array);
        // 倒着取
        String str = "";
        for (int i = array.length - 1; i >= 0; i--) {
            str += String.valueOf(array[i]);
        }
        Integer result = Integer.valueOf(str);
        System.out.println(result);
    }
}
