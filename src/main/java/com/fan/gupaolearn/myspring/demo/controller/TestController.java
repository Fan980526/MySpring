package com.fan.gupaolearn.myspring.demo.controller;

import com.fan.gupaolearn.myspring.demo.service.TestService;
import com.fan.gupaolearn.myspring.framework.annotation.MyAutowired;
import com.fan.gupaolearn.myspring.framework.annotation.MyController;
import com.fan.gupaolearn.myspring.framework.annotation.MyRequestMapping;
import com.fan.gupaolearn.myspring.framework.annotation.MyRequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@MyController()
@MyRequestMapping("/base")
public class TestController {

    @MyAutowired
    private TestService testService;

    @MyRequestMapping("/hello")
    public void hello(HttpServletRequest req, HttpServletResponse resp, @MyRequestParam("name") String name) {
        String hello = testService.Hello(name);
        try {
            resp.setCharacterEncoding("utf-8");
            resp.setContentType("text/html;charset=utf-8");
            resp.getWriter().write(hello);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @MyRequestMapping("/hello")
    public void hello2(HttpServletRequest req, HttpServletResponse resp, @MyRequestParam("name") String name) {
        String hello = testService.Hello(name);
        try {
            resp.setCharacterEncoding("utf-8");
            resp.setContentType("text/html;charset=utf-8");
            resp.getWriter().write(hello);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
