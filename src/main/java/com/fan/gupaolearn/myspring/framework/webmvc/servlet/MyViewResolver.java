package com.fan.gupaolearn.myspring.framework.webmvc.servlet;

import java.io.File;

public class MyViewResolver {

    private final String DEFAULT_TEMPLATE_SUFFIX = ".html";

    private File templateRootDir;

    public MyViewResolver(File templateRootDir) {
        this.templateRootDir = templateRootDir;
    }

    public MyView resolveViewName(String viewName) {
        if (viewName == null || "".equals(viewName.trim())){
            return null;
        }
        viewName = viewName.endsWith(DEFAULT_TEMPLATE_SUFFIX) ? viewName : (viewName + DEFAULT_TEMPLATE_SUFFIX);
        File templateFile = new File((templateRootDir.getParent() + "/" + viewName).replaceAll("/+", "/"));
        return new MyView(templateFile);
    }
}

