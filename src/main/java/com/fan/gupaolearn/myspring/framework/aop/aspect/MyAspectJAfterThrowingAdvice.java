package com.fan.gupaolearn.myspring.framework.aop.aspect;

import com.fan.gupaolearn.myspring.framework.aop.intercept.MyMethodInterceptor;
import com.fan.gupaolearn.myspring.framework.aop.intercept.MyMethodInvocation;


import java.lang.reflect.Method;

/**
 *
 */
public class MyAspectJAfterThrowingAdvice extends MyAbstractAspectJAdvice implements MyMethodInterceptor {

    private String throwName;

    public MyAspectJAfterThrowingAdvice(Object aspect, Method adviceMethod) {
        super(aspect, adviceMethod);
    }

    @Override
    public Object invoke(MyMethodInvocation mi) throws Throwable {
        try {
            return mi.proceed();
        }
        catch (Throwable ex) {
            invokeAdviceMethod(mi, null, ex.getCause());
            throw ex;
        }
    }

    public void setThrowName(String throwName) {
        this.throwName = throwName;
    }
}
