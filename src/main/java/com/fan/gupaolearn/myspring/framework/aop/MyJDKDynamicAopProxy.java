package com.fan.gupaolearn.myspring.framework.aop;

import com.fan.gupaolearn.myspring.framework.aop.aspect.MyAdvice;
import com.fan.gupaolearn.myspring.framework.aop.intercept.MyMethodInvocation;
import com.fan.gupaolearn.myspring.framework.aop.support.MyAdvisedSupport;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * 这里主要是调用 {@link MyAdvisedSupport#getInterceptorsAndDynamicInterceptionAdvice(Method, Class)} 获取拦截器链。
 * 每个被增强的目标方法都对应一个拦截器链
 */
public class MyJDKDynamicAopProxy implements MyAopProxy, InvocationHandler {

    private MyAdvisedSupport advised;

    public MyJDKDynamicAopProxy(MyAdvisedSupport config) {
        this.advised = config;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        List<Object> chain = this.advised.getInterceptorsAndDynamicInterceptionAdvice(method, this.advised.getTargetClass());
        MyMethodInvocation invocation = new MyMethodInvocation(proxy, this.advised.getTarget(), method, args, this.advised.getTargetClass(), chain);
        return invocation.proceed();
    }

    public Object getProxy() {
        return Proxy.newProxyInstance(
                this.getClass().getClassLoader(),
                this.advised.getTargetClass().getInterfaces(),
                this);
    }

    @Override
    public Object getProxy(ClassLoader classLoader) {
        return null;
    }
}
