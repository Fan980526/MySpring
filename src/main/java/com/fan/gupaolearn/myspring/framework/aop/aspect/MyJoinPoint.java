package com.fan.gupaolearn.myspring.framework.aop.aspect;

import java.lang.reflect.Method;

/**
 * 定义一个切点的抽象，这时AOP的基础组成单元。可以理解为一个业务方法的附加信息。
 * 切点应该包含业务方法本身、实参列表和方法所属的实例对象，在可以添加自定义属性。
 * 回调连接点：通过它可以获得被代理的业务方法的所有信息。
 */
public interface MyJoinPoint {

     // 业务方法本身
     Method getMethod();

     // 业务方法的实参列表
     Object[] getArguments();

     // 业务方法所在的实例对象
     Object getThis();

     // 添加自定义属性
     void setUserAttribute(String key, Object value);

     // 从已添加的自定义属性中获取一个属性值
     Object getUserAttribute(String key);
}