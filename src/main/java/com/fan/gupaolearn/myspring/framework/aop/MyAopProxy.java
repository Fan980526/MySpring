package com.fan.gupaolearn.myspring.framework.aop;

/**
 * AopProxy是代理工厂的顶层接口
 */
public interface MyAopProxy {

    // 获得一个代理对象
    Object getProxy();

    // 通过自定义类加载获得一个代理对象
    Object getProxy(ClassLoader classLoader);
}
