package com.fan.gupaolearn.myspring.framework.context;

import com.fan.gupaolearn.myspring.framework.annotation.MyAutowired;
import com.fan.gupaolearn.myspring.framework.annotation.MyController;
import com.fan.gupaolearn.myspring.framework.annotation.MyService;
import com.fan.gupaolearn.myspring.framework.aop.config.MyAopConfig;
import com.fan.gupaolearn.myspring.framework.aop.support.MyAdvisedSupport;
import com.fan.gupaolearn.myspring.framework.aop.MyJDKDynamicAopProxy;
import com.fan.gupaolearn.myspring.framework.beans.MyBeanWrapper;
import com.fan.gupaolearn.myspring.framework.beans.config.MyBeanDefinition;
import com.fan.gupaolearn.myspring.framework.beans.support.MyBeanDefinitionReader;
import com.fan.gupaolearn.myspring.framework.core.MyBeanFactory;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * 负责 Bean 的创建和 DI
 * 依赖 MyBeanDefinitionReader 的支持，完成配置文件的解读和类的扫描，完成类配置缓存 beanDefinitionMap 的填充
 * 根据 beanDefinitionMap 在通过 doAutowrited() -> getBean() 完成 ioc容器 factoryBeanInstanceCahe 的填充
 * MyApplicationContext 中经历了 ： properties -> beanName -> beanDefinition -> beanWrapper -> ioc
 */
public class MyApplicationContext implements MyBeanFactory {

    private MyBeanDefinitionReader reader;

    private Map<String, MyBeanDefinition> beanDefinitionMap = new HashMap<>();

    private Map<String, MyBeanWrapper> factoryBeanInstanceCahe = new HashMap<>();
    // Spring中还会有一个容器来保存纯单例对象
    private Map<String, Object> factoryBeanObjectCahe = new HashMap<>();

    public MyApplicationContext(String... configLocations) {
        try {
            // 1.加载配置文件
            reader = new MyBeanDefinitionReader(configLocations);
            // 2.解析配置文件，扫描相关的类，并把这些类包装成 BeanDefinition
            List<MyBeanDefinition> beanDefinitions = reader.loadBeanDefinitions();
            // 3.把 BeanDefinition 缓存起来
            doRegistBeanDefinition(beanDefinitions);
            // 4.开始准备DI
            doAutowrited();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void doRegistBeanDefinition(List<MyBeanDefinition> beanDefinitions) throws Exception {
        for (MyBeanDefinition beanDefinition : beanDefinitions) {
            if (beanDefinitionMap.containsKey(beanDefinition.getFactoryBeanName())) {
                throw new Exception("The " + beanDefinition.getFactoryBeanName() + " is exists!");
            }
            beanDefinitionMap.put(beanDefinition.getBeanClassName(), beanDefinition);
            beanDefinitionMap.put(beanDefinition.getFactoryBeanName(), beanDefinition);
        }
    }

    /**
     * 到这一步，所有的类还只是处理配置阶段，并没有实例化。
     * 通过调用 getBean() 才开始对类进行实例化，并注入 -> DI 操作
     */
    private void doAutowrited() {
        for (Map.Entry<String, MyBeanDefinition> beanDefinitionEntry : beanDefinitionMap.entrySet()) {
            String beanName = beanDefinitionEntry.getKey();
            getBean(beanName);
        }
        // populateBean 依赖注入中的循环依赖问题可以在这里通过嵌套循环解决，在不甚就递归！想套几层套几层！
    }

    @Override
    public Object getBean(Class beanClass) {
        return this.getBean(beanClass.getName());
    }

    /**
     * Bean的实例化，DI是从这个地方开始的！
     * getBean 完成了 IOC容器的填充
     * 过程：beanName -> beanDefinition -> beanWrapper -> ioc
     */
    @Override
    public Object getBean(String beanName) {
        // 1.先拿到 BeanDefinition 配置
        MyBeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
        // 2.反射实例化 -> 构建对象时，也可做AOP的增强
        Object instance = instantiateBean(beanName, beanDefinition);
        // 3.根据实例化出来的 Bean 封装成一个 BeanWrapper
        MyBeanWrapper beanWrapper = new MyBeanWrapper(instance);
        // 4.将 beanWrapper 保存到 IOC 容器中
        factoryBeanInstanceCahe.put(beanName, beanWrapper);
        // 5.执行 DI 依赖注入
        populateBean(beanName, beanDefinition, beanWrapper);
        return beanWrapper.getWrapperInstance();
    }

    /**
     * 依赖注入
     * 可能会涉及到循环依赖
     */
    private void populateBean(String beanName, MyBeanDefinition beanDefinition, MyBeanWrapper beanWrapper) {
        // 拿到实例与Class
        Object instance = beanWrapper.getWrapperInstance();
        Class<?> clazz = beanWrapper.getWrapperClass();
        //  在 Spring 中都是 @Component :  @Controller和@Service都是@Component的子类
        if (!clazz.isAnnotationPresent(MyController.class) || !clazz.isAnnotationPresent(MyService.class)) {
            return;
        }

        // 获取类中的所有参数 getDeclaredFields()  包含 private/protected/default/public
        for (Field field : clazz.getDeclaredFields()) {
            if (!field.isAnnotationPresent(MyAutowired.class)) {
                continue;
            }
            String autowiredBeanName = field.getAnnotation(MyAutowired.class).value();
            if ("".equals(autowiredBeanName.trim())) {
                // 如果没有自定义beanName,就默认根据类型注入
                autowiredBeanName = field.getType().getName();
            }
            field.setAccessible(true);
            try {
                if (!factoryBeanInstanceCahe.containsKey(autowiredBeanName)) {
                    continue;
                }
                // ioc.get(beanName) 相当于通过接口的全名拿到接口的实现的实例
                field.set(instance, factoryBeanInstanceCahe.get(autowiredBeanName).getWrapperInstance());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 创建真正的实例对象
     */
    private Object instantiateBean(String beanName, MyBeanDefinition beanDefinition) {
        String classsName = beanDefinition.getBeanClassName();
        Object instance = null;
        try {
            // 如果容器中存在，就直接拿，如果没有就创建。 -> 针对 doAutowrited() 解决循环依赖时的 递归调用
            if (this.factoryBeanInstanceCahe.containsKey(beanName)) {
                instance = factoryBeanInstanceCahe.get(beanName);
            } else {
                Class<?> clazz = Class.forName(classsName);
                instance = clazz.newInstance();

                // =======================================AOP开始 TODO AOP可以更优雅的实现
                // 如果满足AOP条件，就做成增强类 -> 直接返回 Proxy 增强的代理对象；
                // 1.加载AOP配置文件
                MyAdvisedSupport aopConfig = instantionAopConfig();
                aopConfig.setTargetClass(clazz);
                aopConfig.setTarget(instance);
                // 2.判断规则 要不要生成代理类，如果要就覆盖原生对象
                if (aopConfig.pointCutMath()) {
                    instance = new MyJDKDynamicAopProxy(aopConfig).getProxy();
                }
                // =======================================AOP结束

                this.factoryBeanObjectCahe.put(beanName, instance);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instance;
    }

    /**
     * 获取 AOP 的配置信息
     */
    private MyAdvisedSupport instantionAopConfig() {
        MyAopConfig aopConfig = new MyAopConfig();
        aopConfig.setPointCut(this.reader.getConfig().getProperty("pointCut"));
        aopConfig.setAspectClass(this.reader.getConfig().getProperty("aspectClass"));
        aopConfig.setAspectBefore(this.reader.getConfig().getProperty("aspectBefore"));
        aopConfig.setAspectAfter(this.reader.getConfig().getProperty("aspectAfter"));
        aopConfig.setAspectAfterThrow(this.reader.getConfig().getProperty("aspectAfterThrow"));
        aopConfig.setAspectAfterThrowingName(this.reader.getConfig().getProperty("aspectAfterThrowingName"));
        return new MyAdvisedSupport(aopConfig);
    }

    public int getBeanDefinitionCount() {
        return this.beanDefinitionMap.size();
    }

    public String[] getBeanDefinitionNames() {
        return this.beanDefinitionMap.keySet().toArray(new String[this.beanDefinitionMap.size()]);
    }

    public Properties getConfig() {
        return this.reader.getConfig();
    }
}
