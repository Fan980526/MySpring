package com.fan.gupaolearn.myspring.framework.aop.support;

import com.fan.gupaolearn.myspring.framework.aop.aspect.MyAdvice;
import com.fan.gupaolearn.myspring.framework.aop.aspect.MyAfterReturningAdviceInterceptor;
import com.fan.gupaolearn.myspring.framework.aop.aspect.MyAspectJAfterThrowingAdvice;
import com.fan.gupaolearn.myspring.framework.aop.aspect.MyMethodBeforeAdviceInterceptor;
import com.fan.gupaolearn.myspring.framework.aop.config.MyAopConfig;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * AdvisedSupport主要是对AOP配置的解析和封装。
 * pointCutMatch()方法用于判断目标类是否符合切面类规则，从而决定是否生成代理类，对目标方法进行增强。
 * getInterceptorsAndDynamicInterceptionAdvice()方法是根据AOP配置，将需要回调的方法封装成一个拦截器链，并返回提供给外部获取。
 */
public class MyAdvisedSupport {

    // AOP的配置信息
    private MyAopConfig aopConfig;

    // 目标类
    private Object target;

    // 目标类的Class
    private Class<?> targetClass;

    // 切面方法的增强方法缓和集合
    private Map<Method, List<Object>> methodCache;

    // 匹配类是否负责AOP配置规则的 正则表达式
    private Pattern pointCutClassPattern;

    public MyAdvisedSupport(MyAopConfig aopConfig) {
        this.aopConfig = aopConfig;
    }


    //根据一个目标代理类的方法，获得其对应的通知 链
    public List<Object> getInterceptorsAndDynamicInterceptionAdvice(Method method, Class<?> targetClass) throws Exception {
        return null;
    }

    // 根据目标代理类的方法获取其相对应的增强方法
    public List<Object> getAdvices(Method method, Object o) throws Exception {
        // 获取此方List<Object>法的增强方法集合
        List<Object> cache = methodCache.get(method);
        if (cache == null) {
            Method targetMethod = targetClass.getMethod(method.getName(), method.getParameterTypes());
            cache = methodCache.get(targetMethod);
            this.methodCache.put(targetMethod, cache);
        }
        return cache;
    }

    public boolean pointCutMath() {
        return pointCutClassPattern.matcher(this.targetClass.toString()).matches();
    }

    public void setTarget(Object target) {
        this.target = target;
    }

    public void setTargetClass(Class<?> targetClass) {
        this.targetClass = targetClass;
        // 拿到类之后，去解析，判断这个类是否负责配置的AOP的规则
        try {
            parse();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parse() throws Exception {
        String pointCut = aopConfig.getPointCut()
                .replaceAll("\\.", "\\\\.")
                .replaceAll("\\\\.\\*", ".*")
                .replaceAll("\\(", "\\\\(")
                .replaceAll("\\)", "\\\\)");
        // 解析配置文件中 AOP的类的配置规则
        // 第一段 修饰符 ， 第二段 类名 ，第三段 方法名称和形参列表
        String pointCutForClassRegex = pointCut.substring(0, pointCut.lastIndexOf("\\(") - 4);
        pointCutClassPattern = Pattern.compile("class" +
                pointCutForClassRegex.substring(pointCutForClassRegex.lastIndexOf(" " + 1)));

        Pattern pointCutPattern = Pattern.compile(pointCut);
        // 获取配置的切面类
        Class aspectClass = Class.forName(this.aopConfig.getAspectClass());
        Map<String, Method> aspectMethods = new HashMap<String, Method>();
        for (Method method : aspectClass.getMethods()) {
            aspectMethods.put(method.getName(), method);
        }

        // 以上都是初始化准备工作，下面开始封装 MyAdvice -> 增强方法的包装
        methodCache = new HashMap<Method, List<Object>>();
        for (Method method : this.targetClass.getMethods()) {
            String methodString = method.toString();
            if (methodString.contains("throws")) {
                methodString = methodString.substring(0, methodString.lastIndexOf("throws")).trim();
            }
            Matcher matcher = pointCutPattern.matcher(methodString);
            if (matcher.matches()) {
                List<Object> advices = new LinkedList<>();
                if (!(null == aopConfig.getAspectBefore() || "".equals(aopConfig.getAspectBefore()))) {
                    advices.add(new MyMethodBeforeAdviceInterceptor(aspectClass.newInstance(), aspectMethods.get(aopConfig.getAspectBefore())));
                }
                if (!(null == aopConfig.getAspectAfter() || "".equals(aopConfig.getAspectAfter()))) {
                    advices.add(new MyAfterReturningAdviceInterceptor(aspectClass.newInstance(), aspectMethods.get(aopConfig.getAspectAfter())));
                }
                if (!(null == aopConfig.getAspectAfterThrow() || "".equals(aopConfig.getAspectAfterThrow()))) {
                    MyAspectJAfterThrowingAdvice advice = new MyAspectJAfterThrowingAdvice(aspectClass.newInstance(), aspectMethods.get(aopConfig.getAspectAfterThrow()));
                    advices.add(advice);
                    advice.setThrowName(aopConfig.getAspectAfterThrowingName());
                }
                methodCache.put(method, advices);
            }
        }
    }

    public Object getTarget() {
        return target;
    }

    public Class<?> getTargetClass() {
        return targetClass;
    }
}
