package com.fan.gupaolearn.myspring.framework.beans.config;

/**
 * 用来保存配置文件中的信息，相当于是保存在内存中的配置
 * 用于保存Bena的相关配置信息
 * BeanDefinition 相当于是每个 Bean 的身份证
 * 保存着 Bean 的 “姓名”和“地址”
 */
public class MyBeanDefinition {

    // 姓名？-> ioc容器中储存的Key
    private String factoryBeanName;

    // 地址？-> Bean的全类名
    private String beanClassName;

    public String getFactoryBeanName() {
        return factoryBeanName;
    }

    public void setFactoryBeanName(String factoryBeanName) {
        this.factoryBeanName = factoryBeanName;
    }

    public String getBeanClassName() {
        return beanClassName;
    }

    public void setBeanClassName(String beanClassName) {
        this.beanClassName = beanClassName;
    }
}
