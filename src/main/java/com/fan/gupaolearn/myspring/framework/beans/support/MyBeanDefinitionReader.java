package com.fan.gupaolearn.myspring.framework.beans.support;

import com.fan.gupaolearn.myspring.framework.annotation.MyController;
import com.fan.gupaolearn.myspring.framework.annotation.MyService;
import com.fan.gupaolearn.myspring.framework.beans.config.MyBeanDefinition;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * 读取并解析配置文件，拿到配置需要被扫描的包。
 * 通过 doScanner 扫描包下的符合条件的类（类名），缓存到 regitryBeanClasses 集合中。
 * 通过 loadBeanDefinitions 并将这些类包装成 BeanDefinition {@link MyBeanDefinition}
 * ，返回给 ApplicationContext。 {@link com.fan.gupaolearn.myspring.framework.context.MyApplicationContext}
 */
public class MyBeanDefinitionReader {

    // 保存扫描的结果 - 类名
    private List<String> regitryBeanClasses = new ArrayList<>();

    private Properties contextConfig = new Properties();

    public MyBeanDefinitionReader(String... configLocations) {
        // 暂时写死 取第一个配置文件即可
        doLoadConfig(configLocations[0]);
        doScanner(contextConfig.getProperty("scanPackage"));
    }

    public List<MyBeanDefinition> loadBeanDefinitions() {
        List<MyBeanDefinition> result = new ArrayList<>();
        try {
            for (String className : regitryBeanClasses) {
                Class<?> beanClass = Class.forName(className);
                String beanName = null;
                // 2.自定义 如果类加了Controller或者Service注解在给他实例化  多个包下的同名类，只能通过注解name属性来自定义全局唯一类名
                if (beanClass.isAnnotationPresent(MyController.class)) {
                    beanName = beanClass.getAnnotation(MyController.class).value();
                } else if (beanClass.isAnnotationPresent(MyService.class)) {
                    beanName = beanClass.getAnnotation(MyService.class).value();
                } else {
                    continue;
                }
                // 1.默认是类名首字母小写 如果注解没自定义类名，则使用默认的雷鸣首字母小写
                if ("".equals(beanName.trim())) {
                    beanName = toLowerFirstCase(beanClass.getSimpleName());
                }
                result.add(doCreateBeanDefinition(beanName, beanClass.getName()));
                // 3.接口注入
                for (Class<?> i : beanClass.getInterfaces()) {
                    result.add(doCreateBeanDefinition(toLowerFirstCase(i.getSimpleName()), beanClass.getName()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private MyBeanDefinition doCreateBeanDefinition(String beanName, String beanClassName) {
        MyBeanDefinition beanDefinition = new MyBeanDefinition();
        beanDefinition.setBeanClassName(beanClassName);
        beanDefinition.setFactoryBeanName(beanName);
        return beanDefinition;
    }

    /**
     * 加载配置文件
     */
    private void doLoadConfig(String contextConfigLocation) {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(
                contextConfigLocation.replaceAll("classpath:", ""));
        try {
            contextConfig.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 扫描包下的类
     */
    private void doScanner(String scanPackage) {
        URL url = this.getClass().getClassLoader().getResource("/" + scanPackage.replaceAll("\\.", "/"));
        File classPath = new File(url.getFile());
        // classPath.listFiles() 就是要扫描的包 -> 文件夹  里面就是一个个的class文件
        for (File file : classPath.listFiles()) {
            if (file.isDirectory()) {
                doScanner(scanPackage + "." + file.getName());
            } else {
                if (!file.getName().endsWith(".class")) {
                    continue;
                }
                String className = scanPackage + "." + file.getName().replace(".class", "");
                regitryBeanClasses.add(className);
            }
        }
    }

    private String toLowerFirstCase(String simpleName) {
        char[] chars = simpleName.toCharArray();
        chars[0] += 32;
        return String.valueOf(chars);
    }

    public Properties getConfig() {
        return this.contextConfig;
    }
}
