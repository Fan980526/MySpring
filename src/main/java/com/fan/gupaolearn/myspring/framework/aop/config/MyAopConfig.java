package com.fan.gupaolearn.myspring.framework.aop.config;

import lombok.Data;

/**
 * Aop的配置储存
 */
@Data
public class MyAopConfig {

    // 切面表达式
    private String pointCut;

    // 切面类
    private String aspectClass;

    // 前置通知回调方法
    private String aspectBefore;

    // 后置通知回调方法
    private String aspectAfter;

    // 异常通知回调方法
    private String aspectAfterThrow;

    // 异常类型捕捉
    private String aspectAfterThrowingName;
}
