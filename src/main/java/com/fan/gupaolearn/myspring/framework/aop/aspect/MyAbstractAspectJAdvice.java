package com.fan.gupaolearn.myspring.framework.aop.aspect;

import java.lang.reflect.Method;

/**
 * 使用模板模式设计
 * 封装拦截器回调的通用逻辑，主要封装反射动态调用方法，其子类只需要控制调用顺序即可。
 */
public class MyAbstractAspectJAdvice implements MyAdvice {

    private Object aspect;
    private Method adviceMethod;

    public MyAbstractAspectJAdvice(Object aspect, Method adviceMethod) {
        this.adviceMethod = adviceMethod;
        this.aspect = aspect;
    }

    // 反射动态调用方法
    protected Object invokeAdviceMethod(MyJoinPoint jp, Object returnValue, Throwable t) throws Throwable {
        //LogAspect.before(),LogAspect.after()  ...
        Class<?> [] paramTypes = this.adviceMethod.getParameterTypes();
        if(null == paramTypes || paramTypes.length == 0){
            return this.adviceMethod.invoke(this.aspect);
        }else{
            Object [] args = new Object[paramTypes.length];
            for (int i = 0; i < paramTypes.length; i++) {
                if(paramTypes[i] == MyJoinPoint.class){
                    args[i] = jp;
                }else if(paramTypes[i] == Throwable.class){
                    args[i] = t;
                }else if(paramTypes[i] == Object.class){
                    args[i] = returnValue;
                }
            }
            return this.adviceMethod.invoke(aspect,args);
        }
    }
}
