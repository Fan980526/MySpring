package com.fan.gupaolearn.myspring.framework.webmvc.servlet;

import java.lang.reflect.Method;
import java.util.regex.Pattern;

/**
 * 封装了 URL 和 Method 的对应关系
 * 以及 Method 所在的实例 controller，方便在项目运行调用时候的使用。
 */
public class MyHandlerMapping {

    // url
    private Pattern url;

    // url对应的目标方法
    private Method method;

    // 目标方法所在的实例对象
    private Object controller;

    public MyHandlerMapping(Pattern url, Method method, Object controller) {
        this.url = url;
        this.method = method;
        this.controller = controller;
    }

    public Pattern getUrl() {
        return url;
    }

    public void setUrl(Pattern url) {
        this.url = url;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Object getController() {
        return controller;
    }

    public void setController(Object controller) {
        this.controller = controller;
    }
}
