package com.fan.gupaolearn.myspring.framework.webmvc.servlet;

import com.fan.gupaolearn.myspring.framework.annotation.MyRequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MyHandlerAdapter {

    public MyModelAndView handler(HttpServletRequest request, HttpServletResponse response, MyHandlerMapping handlerMapping) throws Exception {

        // 1.构建保存形参列表
        // 将参数的 名称和位置 作为 kye和value 的关系，保存起来。
        Map<String, Integer> paramIndexMapping = new HashMap<>();
        // 处理带注解的参数 （不带注解的参数不予理睬）
        Method method = handlerMapping.getMethod();
        Annotation[][] ps = method.getParameterAnnotations();
        for (int i = 0; i < ps.length; i++) {
            for (Annotation annotation : ps[i]) {
                String parameterName = "";
                if (annotation instanceof MyRequestParam) {
                    parameterName = ((MyRequestParam) annotation).value();
                }
                if (!"".equals(parameterName.trim())) {
                    paramIndexMapping.put(parameterName, i);
                }
            }
        }
        // HttpServletRequest 和 HttpServletResponse 肯定不带注解需要单独处理
        Class<?>[] parameterTypes = method.getParameterTypes();
        for (int i = 0; i < parameterTypes.length; i++) {
            Class<?> parameterType = parameterTypes[i];
            if (parameterType == HttpServletRequest.class || parameterType == HttpServletResponse.class) {
                paramIndexMapping.put(parameterType.getName(), i);
            }
        }

        // 2.构建实参列表
        // 获取用户通过URL传过来的参数列表
        Map<String, String[]> reqParameterMap = request.getParameterMap();
        // 待填充的实参列表
        Object[] parameterValues = new Object[parameterTypes.length];
        // 填充普通参数
        for (Map.Entry<String, String[]> param : reqParameterMap.entrySet()) {
            String values = Arrays.toString(param.getValue())
                                .replaceAll("\\[|\\]", "")
                                .replaceAll("s+", "");
            if (!paramIndexMapping.containsKey(param.getKey())) {
                continue;
            }
            Integer paramIndex = paramIndexMapping.get(values);
            parameterValues[paramIndex] = caseStringValue(values,parameterTypes[paramIndex]);
        }
        // HttpServletRequest 和 HttpServletResponse 还是需要单独处理
        if(paramIndexMapping.containsKey(HttpServletRequest.class.getName())){
            int index = paramIndexMapping.get(HttpServletRequest.class.getName());
            parameterValues[index] = request;
        }
        if(paramIndexMapping.containsKey(HttpServletResponse.class.getName())){
            int index = paramIndexMapping.get(HttpServletResponse.class.getName());
            parameterValues[index] = response;
        }

        Object result = method.invoke(handlerMapping.getController(), parameterValues);
        if (result == null || result instanceof  Void) {
            return null;
        }
        boolean isModelAndView = handlerMapping.getMethod().getReturnType() == MyModelAndView.class;
        if(isModelAndView){
            return (MyModelAndView)result;
        }
        return null;
    }

    private Object caseStringValue(String values, Class<?> parameterType) {
        if(String.class == parameterType){
            return values;
        }else if(Integer.class == parameterType){
            return Integer.valueOf(values);
        }else if(Double.class == parameterType){
            return Double.valueOf(values);
        }else {
            if(values != null){
                return values;
            }
            return null;
        }
    }
}
