package com.fan.gupaolearn.myspring.framework.webmvc.servlet;


import com.fan.gupaolearn.myspring.framework.annotation.MyController;
import com.fan.gupaolearn.myspring.framework.annotation.MyRequestMapping;
import com.fan.gupaolearn.myspring.framework.context.MyApplicationContext;
import com.sun.deploy.net.MessageHeader;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 委派模式
 * 负责任务调度
 */
public class MyDispatcherServlet extends HttpServlet {

    private MyApplicationContext applicationContext;

    private List<MyHandlerMapping> handlerMappings = new ArrayList<>();

    private Map<MyHandlerMapping, MyHandlerAdapter> handlerAdapters = new HashMap<>();

    private List<MyViewResolver> viewResolvers = new ArrayList<>();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            doDispath(request, response);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                processDispatchResult(request, response, new MyModelAndView("500"));
            } catch (Exception ex) {
                response.getWriter().write("500 Exception ：" + Arrays.toString(e.getStackTrace()));
            }
        }
    }

    /**
     * 通过request获取请求的地址 根据地址找到容器中对应的 Method 执行后并把执行的结果通过response返回出去。
     * 委派模式
     */
    private void doDispath(HttpServletRequest request, HttpServletResponse response) throws Exception{
        // 1.根据 request 中的 url 获取对应的 handlerMapping
        MyHandlerMapping handlerMapping = getHandlerMapping(request);
        if (handlerMapping == null) {
            processDispatchResult(request, response, new MyModelAndView("404"));
            return;
        }
        // 2.根据 handlerMapping 获取一个 handlerAdapter
        MyHandlerAdapter handlerAdapter = getHandlerAdapter(handlerMapping);
        // 3.解析某一个方法的形参和返回值之后，统一封装为ModelAndView对象
        MyModelAndView modelAndView = handlerAdapter.handler(request, response, handlerMapping);
        // 4.把 ModelAndView 变成一个 ViewResolver
        processDispatchResult(request, response, modelAndView);
    }

    private void processDispatchResult(HttpServletRequest request, HttpServletResponse response, MyModelAndView modelAndView) throws Exception {
        if (null == modelAndView || this.viewResolvers.isEmpty()) {
            return;
        }
        for (MyViewResolver viewResolver : this.viewResolvers) {
            MyView myView = viewResolver.resolveViewName(modelAndView.getView());
            if (myView != null) {
                // 开始往浏览器输出了
                myView.render(modelAndView.getModel(),request,response);
                return;
            }
        }
    }

    private MyHandlerAdapter getHandlerAdapter(MyHandlerMapping handlerMapping) {
        if (this.handlerAdapters.isEmpty()) {
            return null;
        }
        return this.handlerAdapters.get(handlerMapping);
    }

    private MyHandlerMapping getHandlerMapping(HttpServletRequest request) {
        if (this.handlerMappings.isEmpty()) {
            return null;
        }
        String requestURI = request.getRequestURI();
        String contextPath = request.getContextPath();
        String url = requestURI.replaceAll(contextPath, "").replaceAll("/+", "/");
        for (MyHandlerMapping handler : this.handlerMappings) {
            Matcher matcher = handler.getUrl().matcher(url);
            if (!matcher.matches()) {
                continue;
            }
            return handler;
        }
        return null;
    }


    /**
     * 初始化入口 这里需要做的都是准备工作
     * 从加载配置文件 -> 根据配置文件扫描类 -> 将的到的类保存起来 -> 将类注入并初始化容器 -> 初始化url与对应的method -> 反射执行
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        // 初始化 Spring核心容器
        applicationContext = new MyApplicationContext(config.getInitParameter("contextConfigLocation"));
        initStrategies(applicationContext);
        System.out.println("MySpring FrameWork is init.");
    }

    private void initStrategies(MyApplicationContext context) {
        // 初始化 handlerMapping
        initHandlerMappings();
        // 初始化参数适配器
        initHandlerAdapters();
        // 初始化视图转换器
        initViewResolvers();
    }

    /**
     * 将页面模板封装为 ViewResolver
     */
    private void initViewResolvers() {
        String templateRoot = this.applicationContext.getConfig().getProperty("templateRoot");
        String templateRootPath = this.getClass().getClassLoader().getResource(templateRoot).getFile();
        File templateRootDir = new File(templateRootPath);
        for (File template : templateRootDir.listFiles()) {
            // TODO 这一步不是很理解还需要深究
            this.viewResolvers.add(new MyViewResolver(template));
        }
    }

    /**
     * 建立 handlerMapping 和 handlerAdapter 的关联关系，保存在 handlerAdapters map集合中
     * 为什么不在 handlerAdapter 里持有一个 handlerMapping 呢？然后用 List<handlerAdapter> 储存
     */
    private void initHandlerAdapters() {
        for (MyHandlerMapping handlerMapping : handlerMappings) {
            this.handlerAdapters.put(handlerMapping,new MyHandlerAdapter());
        }
    }

    /**
     * 将 controller 中通过 @RequestMapping 配置的 url 和其所在的 method
     * 包装成 {@link MyHandlerMapping} 进行关联
     * 并保存对应关系到 handlerMappings 容器中
     */
    private void initHandlerMappings() {
        if (this.applicationContext.getBeanDefinitionCount() == 0) {
            return;
        }
        for (String beanName : this.applicationContext.getBeanDefinitionNames()) {
            Object instance = this.applicationContext.getBean(beanName);
            Class<?> aClass = instance.getClass();
            if (!aClass.isAnnotationPresent(MyController.class)) {
                continue;
            }
            String baseUrl = aClass.getAnnotation(MyController.class).value();
            // 拿到所有的方法 getMethods() 只有 public 的
            for (Method method : aClass.getMethods()) {
                if (!method.isAnnotationPresent(MyRequestMapping.class)) {
                    continue;
                }
                String url = method.getAnnotation(MyRequestMapping.class).value();
                url = ("/" + baseUrl + "/" + url)
                        .replaceAll("\\*", ".*")
                        .replaceAll("/+", "/");
                Pattern pattern = Pattern.compile(url);
                handlerMappings.add(new MyHandlerMapping(pattern, method, instance));
                System.out.println("Mapped : " + url + "," + method);
            }
        }
    }

}
