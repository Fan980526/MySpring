package com.fan.gupaolearn.myspring.framework.core;

/**
 * 单例工厂的顶层设计规范
 */
public interface MyBeanFactory {

    /**
     * 根据 beanName 从 IOC 容器中获取一个实例 Bean
     * @param beanName
     * @return
     */
    public Object getBean(String beanName);

    public Object getBean(Class beanClass);
}
