package com.fan.gupaolearn.myspring.framework.beans;

/**
 * BeanWrapper主要用于封装创建后的对象实例
 * 这里保存着 Bean 实例 以及 Bean 的其他信息
 * 已经从配置阶段走到实例化阶段了 BeanDefinition -> BeanWrapper
 */
public class MyBeanWrapper {

    private Object wrapperInstance;

    private Class<?> wrapperClass;

    public MyBeanWrapper(Object instance) {
        this.wrapperClass = instance.getClass();
        this.wrapperInstance = instance;
    }

    public Object getWrapperInstance() {
        return wrapperInstance;
    }

    public void setWrapperInstance(Object wrapperInstance) {
        this.wrapperInstance = wrapperInstance;
    }

    public Class<?> getWrapperClass() {
        return wrapperClass;
    }

    public void setWrapperClass(Class<?> wrapperClass) {
        this.wrapperClass = wrapperClass;
    }
}
