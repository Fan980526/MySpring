package com.fan.gupaolearn.myspring.framework.aop.intercept;

import com.fan.gupaolearn.myspring.framework.aop.aspect.MyJoinPoint;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 执行拦截器链
 */
public class MyMethodInvocation implements MyJoinPoint {

    // 代理对象
    private final Object proxy;
    // 代理的目标对象
    private final Object target;
    // 代理的目标类
    private Class<?> targetClass;
    // 代理的目标的方法
    private final Method method;
    // 代理的目标方法的参数列表
    private Object[] arguments = new Object[0];
    // 回调方法链
    protected final List<?> interceptorsAndDynamicMethodMatchers;

    // 用来保存自定义属性
    private Map<String, Object> userAttributes = new HashMap<String, Object>();
    // 游标，用来记录执行到了链中的哪个通知
    private int currentInterceptorIndex = -1;

    public MyMethodInvocation(Object proxy, Object target, Method method, Object[] args, Class<?> targetClass, List<Object> chain) {
        this.proxy = proxy;
        this.target = target;
        this.method = method;
        this.arguments = args;
        this.targetClass = targetClass;
        this.interceptorsAndDynamicMethodMatchers = chain;
    }

    /**
     * 链 执行的方法
     * 如果拦截器链为空则说明不需要增强，直接调用目标方法并返回。
     * 如果拦截器链不为空，则将拦截器链中的方法按顺序执行，直到拦截器链中所有方法全部执行完毕。
     */
    public Object proceed() throws Throwable {
        // 如果通知方法链执行完毕，就执行目标方法
        if (this.currentInterceptorIndex == this.interceptorsAndDynamicMethodMatchers.size() - 1) {
            return this.method.invoke(this.target, this.arguments);
        }
        // 根据游标去链中获取，当前需要执行的一个
        Object interceptorOrInterceptionAdvice =
                this.interceptorsAndDynamicMethodMatchers.get(++this.currentInterceptorIndex);
        //如果要动态匹配joinPoint
        if (interceptorOrInterceptionAdvice instanceof MyMethodInterceptor) {
            MyMethodInterceptor mi = (MyMethodInterceptor) interceptorOrInterceptionAdvice;
            return mi.invoke(this);
        } else {
            return proceed();
        }
    }

    @Override
    public Method getMethod() {
        return this.method;
    }

    @Override
    public Object[] getArguments() {
        return this.arguments;
    }

    @Override
    public Object getThis() {
        return this.target;
    }

    @Override
    public void setUserAttribute(String key, Object value) {

    }

    @Override
    public Object getUserAttribute(String key) {
        return null;
    }
}
