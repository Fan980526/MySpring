package com.fan.gupaolearn.myspring.framework.aop.aspect;

import com.fan.gupaolearn.myspring.framework.aop.intercept.MyMethodInterceptor;
import com.fan.gupaolearn.myspring.framework.aop.intercept.MyMethodInvocation;

import java.lang.reflect.Method;

/**
 *
 */
public class MyMethodBeforeAdviceInterceptor extends MyAbstractAspectJAdvice implements MyMethodInterceptor {

    private MyJoinPoint jp;

    public MyMethodBeforeAdviceInterceptor(Object aspect, Method adviceMethod) {
        super(aspect, adviceMethod);
    }

    public void before(Method method, Object[] args, Object target) throws Throwable {
        this.invokeAdviceMethod(this.jp,null,null);
    }

    @Override
    public Object invoke(MyMethodInvocation mi) throws Throwable {
        jp = mi;
        this.before(mi.getMethod(), mi.getArguments(), mi.getThis());
        return mi.proceed();
    }
}
