package com.fan.gupaolearn.myspring.framework.aop.aspect;

import com.fan.gupaolearn.myspring.framework.aop.intercept.MyMethodInterceptor;
import com.fan.gupaolearn.myspring.framework.aop.intercept.MyMethodInvocation;

import java.lang.reflect.Method;

/**
 *
 */
public class MyAfterReturningAdviceInterceptor extends MyAbstractAspectJAdvice implements MyMethodInterceptor {

    private MyJoinPoint jp;
    public MyAfterReturningAdviceInterceptor(Object aspect, Method adviceMethod) {
        super(aspect, adviceMethod);
    }

    public void afterReturning(Object returnValue, Method method, Object[] args,Object target) throws Throwable {
        invokeAdviceMethod(this.jp, returnValue, null);
    }

    @Override
    public Object invoke(MyMethodInvocation mi) throws Throwable {
        Object returnValue = mi.proceed();
        this.afterReturning(returnValue,mi.getMethod(),mi.getArguments(),mi.getThis());
        return returnValue;
    }

}
