package com.fan.gupaolearn.myspring.framework.webmvc.servlet;

import java.util.Map;

public class MyModelAndView {

    private String view;

    private Map<String, ?> model;

    public MyModelAndView(String view) {
        this.view = view;
    }

    public MyModelAndView(String view, Map<String, ?> model) {
        this.view = view;
        this.model = model;
    }

    public String getView() {
        return view;
    }

    public Map<String, ?> getModel() {
        return model;
    }
}
