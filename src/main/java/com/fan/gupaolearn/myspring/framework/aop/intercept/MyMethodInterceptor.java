package com.fan.gupaolearn.myspring.framework.aop.intercept;

public interface MyMethodInterceptor {

    Object invoke(MyMethodInvocation invocation) throws Throwable;
}
