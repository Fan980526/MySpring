package com.fan.gupaolearn.myspring.framework.aop.aspect;

import lombok.Data;

import java.lang.reflect.Method;

/**
 * 只作为所有回调通知的顶层接口设计不做任何实现，只是一种规范
 */
public interface MyAdvice {

}
